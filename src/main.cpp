#include "gpuErrorCheck.h"
#include <iostream>
#include <fstream>
#include <algorithm>    // std::min
#include <cassert>
#include <iomanip>      // std::setprecision
#include <cuda_profiler_api.h>
#include <cublasXt.h>
using namespace std;

#define SINGLE_GPU
#define BLOCK_DIM 8192

#define hA(i_, j_) ( hA + (i_) + (j_)*ldA )
#define hC(i_, j_) ( hC + (i_) + (j_)*ldC )

__host__ __device__
static inline size_t roundup_to_32X(const size_t x){
    return ((x + 32 - 1) / 32) * 32;
}

// timer
void TimerStart(cudaEvent_t &start){ cudaEventRecord(start, 0); };
void TimerStop(cudaEvent_t &start, cudaEvent_t &stop, float &timer){
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&timer, start, stop);
    //std::cout << "timer=" << timer << std::endl;
    timer /= 1e3; // returns second
};

void print_column_major_matrix(const double *A, const uint64_t M, const uint64_t N){
    cout.precision(1);
    cout << "[" << endl;
    for (uint64_t i = 0; i < M; i++){
        for(uint64_t j = 0; j < N; j++){
            cout  << scientific <<A[j * M + i];
            if(j < N-1) { cout << ", ";      }
            else        { cout <<";" <<endl; }
        }
    }
    cout << "];"<< endl << endl;
}

#include <curand_kernel.h> // for random number generator
#include <time.h>
void genNormalRand(double *h_rand, const size_t m, size_t n){
    
    curandGenerator_t randGen;
    CHECK_CURAND( curandCreateGenerator(&randGen, CURAND_RNG_PSEUDO_DEFAULT) );
    CHECK_CURAND( curandSetPseudoRandomGeneratorSeed(randGen, time(NULL)) );
    
    const size_t batchSize = pow(2, 30) / sizeof(double);// fixed 1GB data every batch
    const size_t lda = roundup_to_32X( m );
    
    double *d_rand;
    if( m * n < batchSize){
        
        // allocate device
        CHECK_CUDA( cudaMalloc((void**)&d_rand, lda * n * sizeof(double)) );
        // generate double normal distribution with mean = 0.0, stddev = 1.0
        CHECK_CURAND( curandGenerateNormalDouble(randGen, d_rand, lda * n, 0.0f, 1.0f) );
        CHECK_CUDA( cudaMemcpy(h_rand, d_rand, m * n * sizeof(double), cudaMemcpyDeviceToHost) );
        
    }else{
        
        CHECK_CUDA( cudaMalloc((void**)&d_rand, batchSize * sizeof(double)) );
        const size_t batch = (m * n) / batchSize;
        for(size_t i = 0; i < batch; i++){
            CHECK_CURAND( curandGenerateNormalDouble(randGen, d_rand, batchSize, 0.0f, 1.0f) );
            CHECK_CUDA( cudaMemcpy(h_rand + i * batchSize, d_rand,
                                   batchSize * sizeof(double), cudaMemcpyDeviceToHost) );
        }
        //last batch
        const size_t lastbatch = m * n - batch * batchSize;
        const size_t roundedlastbatch = roundup_to_32X( lastbatch );
        
        if(lastbatch != 0){
            CHECK_CURAND( curandGenerateNormalDouble(randGen, d_rand, roundedlastbatch, 0.0f, 1.0f) );
            CHECK_CUDA( cudaMemcpy(h_rand + batch * batchSize, d_rand,
                                   lastbatch * sizeof(double), cudaMemcpyDeviceToHost) );
        }
    }
    
    CHECK_CURAND( curandDestroyGenerator(randGen) );
    CHECK_CUDA( cudaFree(d_rand) );
    
}

double frobeniusNorm(const double *h_A, const size_t m, const size_t n){
    
    const size_t batchSize = pow(2, 30) / sizeof(double); // fixed 1GB data
    
    double frobenNorm_A = 0.0;
    double *d_data = NULL;
    cublasHandle_t cublasH = NULL;
    CHECK_CUBLAS( cublasCreate(&cublasH) );
    
    if( m * n < batchSize){ // process in one batch
        // allocate device
        CHECK_CUDA( cudaMalloc((void**)&d_data, m * n * sizeof(double)) );
        
        CHECK_CUDA( cudaMemcpy(d_data, h_A, m * n * sizeof(double), cudaMemcpyHostToDevice) );
        
        CHECK_CUBLAS( cublasDnrm2(cublasH,  m * n, d_data, 1, &frobenNorm_A) );
        
    }else{  // divide and conquer
        double Norm_temp = 0.0;
        CHECK_CUDA( cudaMalloc((void**)&d_data, batchSize * sizeof(double)) );
        const size_t batch = (m * n) / batchSize;
        for(size_t i = 0; i < batch; i++){
            
            CHECK_CUDA( cudaMemcpy(d_data, h_A + i * batchSize,
                                   batchSize * sizeof(double), cudaMemcpyHostToDevice) );
            
            CHECK_CUBLAS( cublasDnrm2(cublasH,  batchSize, d_data, 1, &Norm_temp) );
            
            frobenNorm_A += Norm_temp * Norm_temp;
            
        }
        //last batch
        const size_t lastbatch = m * n - batch * batchSize;
        if(lastbatch != 0){
            CHECK_CUDA( cudaMemcpy(d_data, h_A + batch * batchSize,
                                   lastbatch * sizeof(double), cudaMemcpyHostToDevice) );
            CHECK_CUBLAS( cublasDnrm2(cublasH, lastbatch, d_data, 1, &Norm_temp) );
            frobenNorm_A += Norm_temp * Norm_temp;
        }
        frobenNorm_A = sqrt(frobenNorm_A);
    }
    
    CHECK_CUDA( cudaFree(d_data) );
    CHECK_CUBLAS( cublasDestroy(cublasH) );
    
    //printf("frobenNorm = %0.3f\n", frobenNorm_A);
    return frobenNorm_A;
}

double matrixDiff(const size_t m, const size_t n, double *hA, double *hB, size_t ldA){
    double error = 0.0;
    for (size_t i=0; i < m*n; i++ ){
        error += abs(hA[i]-hB[i]);
    }
    return error;
}

extern "C"
void ts_gemm(const size_t m, const size_t n, const size_t k,
             double &alpha, double* hA, const size_t ldA,
             double *hB, const size_t ldB,
             double &beta, double *hC, const size_t ldC,
             size_t freeMem, const size_t block_size){
    
    //printf("alpha = %f, beta = %f\n", alpha, beta);
    const size_t NUM_STREAM = 2;
    // const double double_one = 1.0, double_zero = 0.0;
    
    /* figure out partition */
    //freeMem *= 0.9;
    //freeMem /= sizeof(double);
    //freeMem -= k * n; // subtract B size
    
    //    size_t mb = freeMem/(n+k);
    //    mb = std::min(mb, m);
    //    size_t mt = (m+mb-1) / mb;
    //int num_stream = std::min(mt, NUM_STREAM);
    int num_stream = 2; // added to find the best size
    size_t mb = block_size; // added to find the best size
    size_t mt = (m+mb-1) / mb;      // # of partition
    //std::cout <<  "mb = " << mb << ", mt = " << mt << std::endl;
    
    const size_t ldda = roundup_to_32X(mb);
    const size_t lddb = roundup_to_32X(k);
    const size_t lddc = roundup_to_32X(mb);
    
    // create stream and cublasHandle
    cudaStream_t cudaStream[num_stream];
    cublasHandle_t  cublasH[num_stream];
    
    // allocate device memory
    double *dA[NUM_STREAM], *dC[NUM_STREAM], *dB = NULL;
    
    for(int i=0; i<num_stream; i++){
        CHECK_CUDA( cudaMalloc((void **)&dA[i], ldda * k * sizeof(double)) );
        CHECK_CUDA( cudaMalloc((void **)&dC[i], lddc * n * sizeof(double)) );
        CHECK_CUDA( cudaStreamCreate(&cudaStream[i]) );
        CHECK_CUBLAS( cublasCreate(&cublasH[i]) );
        CHECK_CUBLAS( cublasSetStream(cublasH[i], cudaStream[i]) );
    }
    
    CHECK_CUDA( cudaMalloc((void **)&dB, lddb * n * sizeof(double)) );
    CHECK_CUBLAS( cublasSetMatrix(k, n, sizeof(double), hB, ldB, dB, lddb));
    
    for(size_t idx=0; idx < mt; idx++){
        int q_num = idx%num_stream;
        size_t mbk = std::min(mb, m - idx*mb);
        //std::cout << "idx = " << idx << ", mbk = " << mbk << std::endl;
        // copy a block of A to device
        CHECK_CUBLAS( cublasSetMatrixAsync(mbk, k, sizeof(double),
                                           hA(idx*mb, 0), ldA,
                                           dA[q_num],     ldda, cudaStream[q_num]) );
        
        CHECK_CUBLAS( cublasSetMatrixAsync(mbk, n, sizeof(double),
                                           hC(idx*mb, 0), ldC,
                                           dC[q_num],     lddc, cudaStream[q_num]) );
        
        // C[i] = A[i] * B
        CHECK_CUBLAS( cublasDgemm(cublasH[q_num], CUBLAS_OP_N, CUBLAS_OP_N,
                                  mbk, n, k, &alpha,
                                  dA[q_num], ldda,
                                  dB,        lddb,  &beta,
                                  dC[q_num], lddc) );
        
        // copy a block of C to host
        CHECK_CUBLAS( cublasGetMatrixAsync(mbk, n, sizeof(double),
                                           dC[q_num],     lddc,
                                           hC(idx*mb, 0), ldC, cudaStream[q_num]) );
        
    }
    
    for (int i=0; i<num_stream; i++) { CHECK_CUDA( cudaStreamSynchronize(cudaStream[i]) );}
    
    //clean up
    for(int i=0; i<num_stream; i++){
        CHECK_CUBLAS( cublasDestroy(cublasH[i]) );
        CHECK_CUDA( cudaStreamDestroy(cudaStream[i]) );
        if (dA[i]) CHECK_CUDA( cudaFree(dA[i]) );
        if (dC[i]) CHECK_CUDA( cudaFree(dC[i]) );
    }
    
    if(dB) CHECK_CUDA( cudaFree(dB) );
}
/**************************** multiGPU****************************************************/
extern "C"
void ts_gemm_multiGPU(const size_t m, const size_t n, const size_t k,
                      double &alpha, double* hA, const size_t ldA,
                      double *hB, const size_t ldB,
                      double &beta, double *hC, const size_t ldC,
                      const size_t block_size){
    
    const int num_stream = 2;
    const size_t mb = block_size;     // added to find the best size
    size_t mt = (m+mb-1) / mb;  // # of partition
    
    const size_t ldda = roundup_to_32X(mb);
    const size_t lddb = roundup_to_32X(k);
    const size_t lddc = roundup_to_32X(mb);
    
    int ngpu = 0;
    cudaGetDeviceCount(&ngpu);
    cudaStream_t cudaStream[ngpu][num_stream];
    cublasHandle_t  cublasH[ngpu][num_stream];
    double *dA[ngpu][num_stream], *dC[ngpu][num_stream], *dB[ngpu];
    
    // create stream and start broasting B to dev
    for(int d = 0; d < ngpu; d++){
        CHECK_CUDA( cudaSetDevice(d) );
        for(int i=0; i<num_stream; i++){
            //CHECK_CUDA( cudaStreamCreate(&cudaStream[d][i]) )
            CHECK_CUDA( cudaStreamCreateWithFlags(&cudaStream[d][i], cudaStreamNonBlocking) );
        }
        CHECK_CUDA( cudaMalloc((void **)&dB[d], lddb * n * sizeof(double)) );
        CHECK_CUBLAS( cublasSetMatrixAsync(k, n, sizeof(double), hB, ldB,
                                           dB[d], lddb, cudaStream[d][0]));
    }
    
    // create cublas handle and bound with stream
    for(int d = 0; d < ngpu; d++){
        CHECK_CUDA( cudaSetDevice(d) );
        for(int i=0; i<num_stream; i++){
            CHECK_CUBLAS( cublasCreate(&cublasH[d][i]) );
            CHECK_CUBLAS( cublasSetStream(cublasH[d][i], cudaStream[d][i]) );
            CHECK_CUDA( cudaMalloc((void **)&dA[d][i], ldda * k * sizeof(double)) );
            CHECK_CUDA( cudaMalloc((void **)&dC[d][i], lddc * n * sizeof(double)) );
        }
    }
    
    for(size_t idx=0; idx < mt; idx++){
        int d = idx % ngpu;
        CHECK_CUDA( cudaSetDevice(d) );
        int q_num = (idx/ngpu)%num_stream;
        size_t mbk = std::min(mb, m - idx*mb);
        //std::cout << "idx = " << idx <<", d = " << d << ", q_num = " << q_num
        //        << ", mbk = " << mbk << std::endl;
        // copy a block of A to device
        CHECK_CUBLAS( cublasSetMatrixAsync(mbk, k, sizeof(double),
                                           hA(idx*mb, 0), ldA,
                                           dA[d][q_num],  ldda, cudaStream[d][q_num]) );
        
        CHECK_CUBLAS( cublasSetMatrixAsync(mbk, n, sizeof(double),
                                           hC(idx*mb, 0), ldC,
                                           dC[d][q_num],  lddc, cudaStream[d][q_num]) );
        
        // C[i] = A[i] * B
        CHECK_CUBLAS( cublasDgemm(cublasH[d][q_num], CUBLAS_OP_N, CUBLAS_OP_N,
                                  mbk, n, k, &alpha,
                                  dA[d][q_num], ldda,
                                  dB[d],        lddb,  &beta,
                                  dC[d][q_num], lddc) );
        
        //print_device_matrix(dA[q_num], mbk, n, ldda, "dA");
        //print_device_matrix(dC[q_num], mbk, k, lddc, "dC");
        
        // copy a block of C to host
        CHECK_CUBLAS( cublasGetMatrixAsync(mbk, n, sizeof(double),
                                           dC[d][q_num],  lddc,
                                           hC(idx*mb, 0), ldC, cudaStream[d][q_num]) );
        
    }
    
    for(int d = 0; d < ngpu; d++){
        CHECK_CUDA( cudaSetDevice(d) );
        for (int i=0; i<num_stream; i++)
        { CHECK_CUDA( cudaStreamSynchronize(cudaStream[d][i]) );}
        
        //clean up
        for(int i=0; i<num_stream; i++){
            CHECK_CUBLAS( cublasDestroy(cublasH[d][i]) );
            CHECK_CUDA( cudaStreamDestroy(cudaStream[d][i]) );
            if (dA[i]) CHECK_CUDA( cudaFree(dA[d][i]) );
            if (dC[i]) CHECK_CUDA( cudaFree(dC[d][i]) );
        }
        
        if(dB[d]) CHECK_CUDA( cudaFree(dB[d]) );
        CHECK_CUDA(cudaDeviceSynchronize());
    }
    
    cudaSetDevice(0);
}

/********************************** short-wide A: C = A^T * B **********************************/
// A : m-by-n, B: m-by-k, C:n-by-k, m << n
extern "C"
void sw_trans_gemm_multiGPU(const size_t n, const size_t k, const size_t m,
                            double &alpha, double* hA, const size_t ldA,
                            double *hB, const size_t ldB,
                            double &beta, double *hC, const size_t ldC,
                            const size_t block_size){
    
    const int num_stream = 2;
    const size_t nb = block_size;     // added to find the best size
    size_t nt = (n+nb-1) / nb;  // # of partition
    
    const size_t ldda = roundup_to_32X( m );
    const size_t lddb = roundup_to_32X( m );
    const size_t lddc = roundup_to_32X( nb );
    
    int ngpu = 0;
    cudaGetDeviceCount(&ngpu);
    cudaStream_t cudaStream[ngpu][num_stream];
    cublasHandle_t  cublasH[ngpu][num_stream];
    double *dA[ngpu][num_stream], *dC[ngpu][num_stream], *dB[ngpu];
    
    // create stream and start broadcasting B to dev
    for(int d = 0; d < ngpu; d++){
        CHECK_CUDA( cudaSetDevice(d) );
        for(int i=0; i<num_stream; i++){
            //CHECK_CUDA( cudaStreamCreate(&cudaStream[d][i]) )
            CHECK_CUDA( cudaStreamCreateWithFlags(&cudaStream[d][i], cudaStreamNonBlocking) );
        }
        CHECK_CUDA( cudaMalloc((void **)&dB[d], lddb * k * sizeof(double)) );
        CHECK_CUBLAS( cublasSetMatrixAsync(m, k, sizeof(double), hB, ldB,
                                           dB[d], lddb, cudaStream[d][0]));
    }
    
    // create cublas handle and bound with stream
    for(int d = 0; d < ngpu; d++){
        CHECK_CUDA( cudaSetDevice(d) );
        for(int i=0; i<num_stream; i++){
            CHECK_CUBLAS( cublasCreate(&cublasH[d][i]) );
            CHECK_CUBLAS( cublasSetStream(cublasH[d][i], cudaStream[d][i]) );
            CHECK_CUDA( cudaMalloc((void **)&dA[d][i], ldda * nt * sizeof(double)) );
            CHECK_CUDA( cudaMalloc((void **)&dC[d][i], lddc * k * sizeof(double)) );
        }
    }
    
    for(size_t idx=0; idx < nt; idx++){
        int d = idx % ngpu;
        CHECK_CUDA( cudaSetDevice(d) );
        int q_num = (idx/ngpu)%num_stream;
        size_t nbk = std::min(nb, n - idx*nb);
        std::cout << "idx = " << idx <<", d = " << d << ", q_num = " << q_num
        << ", nbk = " << nbk << std::endl;
        // copy a block of A to device
        CHECK_CUBLAS( cublasSetMatrixAsync(m, nt, sizeof(double),
                                           hA(0, idx*nb), ldA,
                                           dA[d][q_num],  ldda, cudaStream[d][q_num]) );
        
        CHECK_CUBLAS( cublasSetMatrixAsync(nbk, k, sizeof(double),
                                           hC(idx*nb, 0), ldC,
                                           dC[d][q_num],  lddc, cudaStream[d][q_num]) );
        
        // C[i] = A[i]^T * B
        CHECK_CUBLAS( cublasDgemm(cublasH[d][q_num], CUBLAS_OP_T, CUBLAS_OP_N,
                                  nt, k, m, &alpha,
                                  dA[d][q_num], ldda,
                                  dB[d],        lddb,  &beta,
                                  dC[d][q_num], lddc) );
        
        //print_device_matrix(dA[q_num], mbk, n, ldda, "dA");
        //print_device_matrix(dC[q_num], mbk, k, lddc, "dC");
        
        // copy the block of C to host
        CHECK_CUBLAS( cublasGetMatrixAsync(nbk, k, sizeof(double),
                                           dC[d][q_num],  lddc,
                                           hC(idx*nb, 0), ldC, cudaStream[d][q_num]) );
        
    }
    
    for(int d = 0; d < ngpu; d++){
        CHECK_CUDA( cudaSetDevice(d) );
        for (int i=0; i<num_stream; i++)
        { CHECK_CUDA( cudaStreamSynchronize(cudaStream[d][i]) );}
        
        //clean up
        for(int i=0; i<num_stream; i++){
            CHECK_CUBLAS( cublasDestroy(cublasH[d][i]) );
            CHECK_CUDA( cudaStreamDestroy(cudaStream[d][i]) );
            if (dA[i]) CHECK_CUDA( cudaFree(dA[d][i]) );
            if (dC[i]) CHECK_CUDA( cudaFree(dC[d][i]) );
        }
        
        if(dB[d]) CHECK_CUDA( cudaFree(dB[d]) );
        CHECK_CUDA(cudaDeviceSynchronize());
    }
    
    cudaSetDevice(0);
}

extern "C"
void ts_gemm_test(const size_t m, const size_t n, const size_t k){
    
    // timer setup
    float singleGPUtime = 0.0, mgpu_time = 0.0, XTpin_time = 0.0,
    XTuma_time = 0.0, froErr = 0.0;
    cudaEvent_t  start0, stop0;
    CHECK_CUDA( cudaEventCreate(&start0) );
    CHECK_CUDA( cudaEventCreate(&stop0) );
    
    // allocate host memory as pinned memory
    const size_t ldA = m;
    const size_t ldB = k;
    const size_t ldC = m;
    double *hA = NULL, *hB = NULL, *hC = NULL;
    
    CHECK_CUDA( cudaMallocHost((void**)&hA, m * k * sizeof(double)) );
    CHECK_CUDA( cudaMallocHost((void**)&hB, k * n * sizeof(double)) );
    CHECK_CUDA( cudaMallocHost((void**)&hC, m * n * sizeof(double)) );
    
    genNormalRand(hA, m, k);
    genNormalRand(hB, k, n);
    genNormalRand(hC, m, n);
    
    double alpha = 2.394, beta = 4.194;
    int block_size = 8192;
    if (m < 40000) {block_size = 1024;}
    
    /***********************warm up *******************************/
    ts_gemm_multiGPU(m, n, k,
                     alpha, hA, ldA,
                     hB, ldB,
                     beta, hC, ldC, block_size);
    /********************************************************************/
    CHECK_CUDA(cudaDeviceSynchronize());
    
    /****************************Single GPU ********************************/
    size_t freeMem, totalMem;
    cudaMemGetInfo( &freeMem, &totalMem );
    TimerStart(start0);
    ts_gemm(m, n, k,
            alpha, hA, ldA,
            hB, ldB,
            beta, hC, ldC, freeMem, block_size);
    TimerStop(start0, stop0, singleGPUtime);
    
    /**************************** multi-GPU ***********************************/
    TimerStart(start0);
    ts_gemm_multiGPU(m, n, k, alpha, hA, ldA, hB, ldB, beta, hC, ldC, block_size);
    CHECK_CUDA(cudaDeviceSynchronize());
    
    //cudaProfilerStop();
    TimerStop(start0, stop0, mgpu_time);
    
    /************************ cublas XT with Pinned memroy ***********************/
    TimerStart(start0);
    // setup device
    int ngpu = 0;
    cudaGetDeviceCount(&ngpu);
    int devices[8] = { 0, 1, 2, 3, 4, 5, 6, 7};
    cublasXtHandle_t cublasXtH = NULL;
    cublasXtCreate(&cublasXtH);
#ifdef SINGLE_GPU
    ngpu = 1;
#endif
    CHECK_CUBLAS( cublasXtDeviceSelect(cublasXtH, ngpu, devices) );
    // set block dimension to k;
    size_t xtBlockDim = 8192;
    if (m < xtBlockDim){ // the threshold is decided by experiments on V100
        xtBlockDim = std::max(size_t(1024), n);
    }else if(m < xtBlockDim * 2){
        xtBlockDim = std::max(size_t(4096), n);
    }
    CHECK_CUBLAS( cublasXtSetBlockDim(cublasXtH, xtBlockDim) );
    CHECK_CUBLAS( cublasXtDgemm(cublasXtH, CUBLAS_OP_N, CUBLAS_OP_N,
                                m, n, k,
                                &alpha,
                                hA, ldA,
                                hB, ldB,
                                &beta,
                                hC, ldC) );
    
    CHECK_CUBLAS( cublasXtDestroy(cublasXtH) );
    CHECK_CUDA(cudaDeviceSynchronize());
    TimerStop(start0, stop0, XTpin_time);
    
    //froErr = matrixDiff(m, n, hC, hC2, ldA);
    
    if(hA) CHECK_CUDA( cudaFreeHost(hA) );
    if(hB) CHECK_CUDA( cudaFreeHost(hB) );
    if(hC) CHECK_CUDA( cudaFreeHost(hC) );
    //   if(hC2) CHECK_CUDA( cudaFreeHost(hC2) );
    //    free( hA );
    //    free( hB );
    //    free( hC );
    
    /*************************  cublas XT with UMA *******************************/
    //    CHECK_CUDA( cudaMallocManaged((void**)&hA, m * k * sizeof(double)) );
    //    CHECK_CUDA( cudaMallocManaged((void**)&hB, k * n * sizeof(double)) );
    //    CHECK_CUDA( cudaMallocManaged((void**)&hC, m * n * sizeof(double)) );
    //
    //    genNormalRand(hA, m, k);
    //    genNormalRand(hB, k, n);
    //    genNormalRand(hC, m, n);
    //
    //    TimerStart(start0);
    //    //cublasXtHandle_t cublasXtH = NULL;
    //    cublasXtCreate(&cublasXtH);
    //    // setup device
    ////    int ngpu = 0;
    ////    cudaGetDeviceCount(&ngpu);
    ////    int devices[8] = { 0, 1, 2, 3, 4, 5, 6, 7};
    ////
    //#ifdef SINGLE_GPU
    //    ngpu = 1;
    //#endif
    //    CHECK_CUBLAS( cublasXtDeviceSelect(cublasXtH, ngpu, devices) );
    //    CHECK_CUBLAS( cublasXtSetBlockDim(cublasXtH, xtBlockDim) );
    //    CHECK_CUBLAS( cublasXtDgemm(cublasXtH, CUBLAS_OP_N, CUBLAS_OP_N,
    //                                m, n, k,
    //                                &alpha,
    //                                hA, ldA,
    //                                hB, ldB,
    //                                &beta,
    //                                hC, ldC) );
    //
    //    CHECK_CUBLAS( cublasXtDestroy(cublasXtH) );
    //    CHECK_CUDA(cudaDeviceSynchronize());
    //    TimerStop(start0, stop0, XTuma_time);
    
    // check error
    
    //    if(hA) CHECK_CUDA( cudaFree(hA) );
    //    if(hB) CHECK_CUDA( cudaFree(hB) );
    //    if(hC) CHECK_CUDA( cudaFree(hC) );
    
    // clean up
    CHECK_CUDA( cudaEventDestroy(start0) );
    CHECK_CUDA( cudaEventDestroy(stop0) );
    
    const double Tflops = double(m * n * k * 2) / 1e12;
    const double single_perf = Tflops / singleGPUtime;
    const double XTpin_perf  = Tflops / XTpin_time;
    const double XTuma_perf  = Tflops / XTuma_time;
    const double mgpu_perf   = Tflops / mgpu_time;
    
    // print to screen
    double dataGB = m * k * sizeof(double) / pow(2, 30);
    std::cout
    << dataGB << "\t"<< m << "\t" << n << "\t" << k << "\t"
    << std::setprecision(2)
    << singleGPUtime << "\t" << single_perf << "\t"
    << mgpu_time << "\t" <<  mgpu_perf      << "\t"
    << XTpin_time    << "\t" << XTpin_perf  << "\t"
    << XTuma_time    << "\t" << XTuma_perf  << "\t"
    << froErr << std::endl;
    
    //save to file
    fstream fs;
    string fileName = "multi_perf.csv";
#ifdef SINGLE_GPU
    fileName = "single_perf.csv";
#endif
    fs.open(fileName, fstream::out | fstream::app);
    fs << dataGB << "," << m << "," << n << "," << k << ","
    << singleGPUtime << "," << single_perf << ","
    << mgpu_time << "," <<  mgpu_perf      << ","
    << XTpin_time    << "," << XTpin_perf  << ","
    << XTuma_time    << "," << XTuma_perf  << ","
    << froErr << std::endl;
    fs.close();
    
}

/********************************* short-wide test *******************************/
// A : m-by-n, B: m-by-k, C:n-by-k
extern "C"
void sw_gemm_test(const size_t n, const size_t k, const size_t m){
    
    // timer setup
    float singleGPUtime = 0.0, mgpu_time = 0.0, XTpin_time = 0.0,
    XTuma_time = 0.0, froErr = 0.0;
    cudaEvent_t  start0, stop0;
    CHECK_CUDA( cudaEventCreate(&start0) );
    CHECK_CUDA( cudaEventCreate(&stop0) );
    
    // allocate host memory as pinned memory
    const size_t ldA = m;
    const size_t ldB = m;
    const size_t ldC = n;
    double *hA = NULL, *hB = NULL, *hC = NULL;
    
    CHECK_CUDA( cudaMallocHost((void**)&hA, m * n * sizeof(double)) );
    CHECK_CUDA( cudaMallocHost((void**)&hB, m * k * sizeof(double)) );
    CHECK_CUDA( cudaMallocHost((void**)&hC, n * k * sizeof(double)) );
    
    genNormalRand(hA, m, n);
    genNormalRand(hB, m, k);
    genNormalRand(hC, n, k);
    
    double alpha = 2.394, beta = 4.194;
    int block_size = 8192;
    if (m < 40000) {block_size = 1024;}
    
    /***********************warm up *******************************/
    sw_trans_gemm_multiGPU(n, k, m,
                           alpha, hA, ldA,
                           hB, ldB,
                           beta, hC, ldC, block_size);
    /********************************************************************/
    CHECK_CUDA(cudaDeviceSynchronize());
    
    /****************************Single GPU ********************************/
    size_t freeMem, totalMem;
    //cudaMemGetInfo( &freeMem, &totalMem );
    TimerStart(start0);
    //    ts_gemm(m, n, k,
    //            alpha, hA, ldA,
    //            hB, ldB,
    //            beta, hC, ldC, freeMem, block_size);
    TimerStop(start0, stop0, singleGPUtime);
    
    /**************************** multi-GPU ***********************************/
    TimerStart(start0);
    sw_trans_gemm_multiGPU(n, k, m, alpha, hA, ldA, hB, ldB, beta, hC, ldC, block_size);
    CHECK_CUDA(cudaDeviceSynchronize());
    
    //cudaProfilerStop();
    TimerStop(start0, stop0, mgpu_time);
    
    /************************ cublas XT with Pinned memroy ***********************/
//    TimerStart(start0);
//    // setup device
//    int ngpu = 0;
//    cudaGetDeviceCount(&ngpu);
//    int devices[8] = { 0, 1, 2, 3, 4, 5, 6, 7};
//    cublasXtHandle_t cublasXtH = NULL;
//    cublasXtCreate(&cublasXtH);
//#ifdef SINGLE_GPU
//    ngpu = 1;
//#endif
//    CHECK_CUBLAS( cublasXtDeviceSelect(cublasXtH, ngpu, devices) );
//    // set block dimension to k;
//    size_t xtBlockDim = 8192;
//    if (n < xtBlockDim){ // the threshold is decided by experiments on V100
//        xtBlockDim = std::max(size_t(1024), n);
//    }else if(n < xtBlockDim * 2){
//        xtBlockDim = std::max(size_t(4096), n);
//    }
//    CHECK_CUBLAS( cublasXtSetBlockDim(cublasXtH, xtBlockDim) );
//    CHECK_CUBLAS( cublasXtDgemm(cublasXtH, CUBLAS_OP_T, CUBLAS_OP_N,
//                                n, k, m,
//                                &alpha,
//                                hA, ldA,
//                                hB, ldB,
//                                &beta,
//                                hC, ldC) );
//
//    CHECK_CUBLAS( cublasXtDestroy(cublasXtH) );
//    CHECK_CUDA(cudaDeviceSynchronize());
//    TimerStop(start0, stop0, XTpin_time);
//
//    //froErr = matrixDiff(m, n, hC, hC2, ldA);
//
    if(hA) CHECK_CUDA( cudaFreeHost(hA) );
    if(hB) CHECK_CUDA( cudaFreeHost(hB) );
    if(hC) CHECK_CUDA( cudaFreeHost(hC) );
    
    /*************************  cublas XT with UMA *******************************/
    //    CHECK_CUDA( cudaMallocManaged((void**)&hA, m * k * sizeof(double)) );
    //    CHECK_CUDA( cudaMallocManaged((void**)&hB, k * n * sizeof(double)) );
    //    CHECK_CUDA( cudaMallocManaged((void**)&hC, m * n * sizeof(double)) );
    //
    //    genNormalRand(hA, m, k);
    //    genNormalRand(hB, k, n);
    //    genNormalRand(hC, m, n);
    //
    //    TimerStart(start0);
    //    //cublasXtHandle_t cublasXtH = NULL;
    //    cublasXtCreate(&cublasXtH);
    //    // setup device
    ////    int ngpu = 0;
    ////    cudaGetDeviceCount(&ngpu);
    ////    int devices[8] = { 0, 1, 2, 3, 4, 5, 6, 7};
    ////
    //#ifdef SINGLE_GPU
    //    ngpu = 1;
    //#endif
    //    CHECK_CUBLAS( cublasXtDeviceSelect(cublasXtH, ngpu, devices) );
    //    CHECK_CUBLAS( cublasXtSetBlockDim(cublasXtH, xtBlockDim) );
    //    CHECK_CUBLAS( cublasXtDgemm(cublasXtH, CUBLAS_OP_N, CUBLAS_OP_N,
    //                                m, n, k,
    //                                &alpha,
    //                                hA, ldA,
    //                                hB, ldB,
    //                                &beta,
    //                                hC, ldC) );
    //
    //    CHECK_CUBLAS( cublasXtDestroy(cublasXtH) );
    //    CHECK_CUDA(cudaDeviceSynchronize());
    //    TimerStop(start0, stop0, XTuma_time);
    
    // check error
    
    //    if(hA) CHECK_CUDA( cudaFree(hA) );
    //    if(hB) CHECK_CUDA( cudaFree(hB) );
    //    if(hC) CHECK_CUDA( cudaFree(hC) );
    
    // clean up
    CHECK_CUDA( cudaEventDestroy(start0) );
    CHECK_CUDA( cudaEventDestroy(stop0) );
    
    const double Tflops = double(m * n * k * 2) / 1e12;
    const double single_perf = Tflops / singleGPUtime;
    const double XTpin_perf  = Tflops / XTpin_time;
    const double XTuma_perf  = Tflops / XTuma_time;
    const double mgpu_perf   = Tflops / mgpu_time;
    
    // print to screen
    double dataGB = m * k * sizeof(double) / pow(2, 30);
    std::cout
    << dataGB << "\t"<< m << "\t" << n << "\t" << k << "\t"
    << std::setprecision(2)
    << singleGPUtime << "\t" << single_perf << "\t"
    << mgpu_time << "\t" <<  mgpu_perf      << "\t"
    << XTpin_time    << "\t" << XTpin_perf  << "\t"
    << XTuma_time    << "\t" << XTuma_perf  << "\t"
    << froErr << std::endl;
    
    //save to file
    fstream fs;
    string fileName = "multi_sw_perf.csv";
#ifdef SINGLE_GPU
    fileName = "single_sw_perf.csv";
#endif
    fs.open(fileName, fstream::out | fstream::app);
    fs << dataGB << "," << m << "," << n << "," << k << ","
    << singleGPUtime << "," << single_perf << ","
    << mgpu_time << "," <<  mgpu_perf      << ","
    << XTpin_time    << "," << XTpin_perf  << ","
    << XTuma_time    << "," << XTuma_perf  << ","
    << froErr << std::endl;
    fs.close();
    
}

int main(int argc, char **argv) {
    
    size_t m = 0, n = 0, k = 0, pattern = 0;
    const size_t data_num = 20;

    std::cout <<
    "size[GB]  m    n      k       single[s]  Tflop\\s  multi[s] Tflop\\s  XTpin[s]  Tflop\\s XTuma[s]  Tflop\\s   error"
    << std::endl;
    size_t m_array[13]={1000,2024,4072,8168, 16360, 32744, 65512, 131048,
        262120, 524264,1048552, 2097128, 4194280};
    size_t max_iter = 0;
    // test 1
    k = 1000; n = 1000;
    if(k <= 1000){max_iter = 13;}else{max_iter = 11;}
    for(size_t i = 0; i < max_iter; i++){
        m = m_array[i];
        //ts_gemm_test(m, n, k);
        sw_gemm_test(m, n, k);
    }
    //test 2
    k = 5000; n = 5000;
    if(k <= 1000){max_iter = 13;}else{max_iter = 11;}
    for(size_t i = 0; i < max_iter; i++){
        m = m_array[i];
        //ts_gemm_test(m, n, k);
        sw_gemm_test(m, n, k);
    }
    // test 3
    k= 5000; n = 500;
    for(size_t i = 0; i < max_iter; i++){
        m = m_array[i];
        ts_gemm_test(m, n, k);
    }
    // test 4
    k = 1000; n = 10000;
    for(size_t i = 0; i < max_iter; i++){
        m = m_array[i];
        //ts_gemm_test(m, n, k);
        sw_gemm_test(m, n, k);
    }
    
    
    
    return 0;
}

