# Tall-skinny GEMM

CUDA implementation of tall-skinny GEMM.

# Requirements
## Hardware
* Nvidia CUDA compatiable GPU (computation ability > 3.5)

## OS
* Ubuntu 16.04 & CentOS 7.2 & Mac OS X 10.13
* Windows has not been tested yet.

# Install

## Dependnecy
* [CMake] (https://cmake.org/download)
* [CUDA >= 9.0](https://developer.nvidia.com/cuda-downloads)

## Ubuntu 16.04 / CentOS 7.2 / Mac OS X 10.13
* Step 1:
Install CMake, CUDA.

* Step 2:
    1. git clone https://bitbucket.org/luyuechao/ts_gemm/
    2. In the project directory, edit CMakeList.txt file.
    
     change "arch" and "code" compiler flag according to your GPU architecture.
       Refer to https://en.wikipedia.org/wiki/CUDA
     (e.g. GTX TITAN X Maxwell (Compute ability 5.2) Set "arch" and "code" to "arch=compute_52" and "code=sm_52")
    
```
> mkdir build && cd build
> cmake ..
> make -j4
> ./tsGEMM

```
The result will be saved on a .csv file.

